package com.example.Contoller;

import com.example.Model.ActicleModel;
import com.example.Service.ServiceImp.ServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;

@Controller
public class ArticleController {
    @Autowired
    ServiceImp serviceImp;

    public ArticleController(ServiceImp serviceImp) {
        this.serviceImp = serviceImp;
    }

    @RequestMapping("/index")
    public String Homepage(ModelMap modelMap) {
        ArrayList<ActicleModel> object=serviceImp.getAll();
        modelMap.addAttribute("article",object);

        return "Index";
    }
    @GetMapping("/view/{id}")
    public String getview(Model model, @PathVariable("id") Integer id,ActicleModel articleModel){
        articleModel=serviceImp.view(id);
        model.addAttribute("view",articleModel);
        return "View";
    }
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Integer id){
        if(serviceImp.delete(id)){
            System.out.println("delete success !");
        }else
            System.out.println("delete faild !");
        return "redirect:/index";
    }



}
