package com.example.Service;

import com.example.Model.ActicleModel;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
@Service
public interface ArticleService  {
    ArrayList<ActicleModel> getAll();

    ActicleModel view(int id);
    boolean delete(int id);
}
