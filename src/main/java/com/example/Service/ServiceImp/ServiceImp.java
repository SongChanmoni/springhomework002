package com.example.Service.ServiceImp;

import com.example.Model.ActicleModel;
import com.example.Repository.ArticleRepository;
import com.example.Service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ServiceImp implements ArticleService {
    @Autowired
    ArticleRepository articleRepository;
    public ServiceImp(ArticleRepository articleRepository){
        this.articleRepository=articleRepository;

    }


    @Override
    public ArrayList<ActicleModel> getAll() {
        return articleRepository.getAll();
    }

    @Override
    public ActicleModel view(int id) {
        return articleRepository.view(id);
    }

    @Override
    public boolean delete(int id) {
        return articleRepository.delete(id);
    }
}
